# Spring AOP demo

Demo project showing how to use Spring AOP.

Featuring:

 * Joinpoint matching annotated methods
 * Advice using arguments, return value and annotation fields.
 * Lightweight unit test, loading just the 'under test' service and the aspect. Avoiding the load the complete
   spring boot application.
