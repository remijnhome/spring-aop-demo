/*
 * Developers.nl 2018
 */
package hello;

import javax.annotation.PostConstruct;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.AfterReturning;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.stereotype.Component;

/**
 *
 * @author Marc Remijn, m.remijn@developers.nl
 */
@Aspect
@Component
public class AuditAspect {

    private Log log = LogFactory.getLog(this.getClass());

    @Pointcut("execution(String hello..*(..)) && @annotation(auditable)")// the pointcut expression
    public void resolution(Auditable auditable) {
    }

    @Before("hello.AuditAspect.resolution(auditable)")
    public void beforeResolution(Auditable auditable) {
        System.err.println("YES");
        log.info("Voor een resolution");
        log.info("De actie is: " + auditable.actie());
        log.info("De entiteit is: " + auditable.entiteitNaam());
    }

    @AfterReturning(pointcut="hello.AuditAspect.resolution(auditable) && args(arg0,..)", returning="retVal")
    public void afterResolution(String arg0, String retVal, Auditable auditable) {
        log.info(String.format("Method werd aangeroepen met als eerste argument: %s", arg0));
//        log.info(String.format("Method werd aangeroepen op een: %s", joinPoint.getTarget().getClass()));
//        log.info(String.format("Method werd aangeroepen op een proxy: %s", joinPoint.getThis().getClass()));
//        log.info(String.format("Het eerste argument was: %s", joinPoint.getArgs()[0]));
        log.info(String.format("Na een resolution werd  [%s] teruggeven", retVal));
    }

}
