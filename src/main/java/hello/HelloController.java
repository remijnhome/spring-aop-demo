package hello;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

@RestController
public class HelloController {
    
    @Autowired
    private ResolutionService resolutionService;
    
    private Log log = LogFactory.getLog(getClass());
    
    @RequestMapping("/")
    public String index(@RequestParam(defaultValue = "World", name = "name") String name) {
        log.info("/ wordt aangeroepen");
        return resolutionService.resolveGreeting(name);
    }

    
}
