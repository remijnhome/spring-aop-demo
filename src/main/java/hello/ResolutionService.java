/*
 * Developers.nl 2018
 */
package hello;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Service;

/**
 *
 * @author Marc Remijn, m.remijn@developers.nl
 */
@Service
public class ResolutionService {
    private Log log = LogFactory.getLog(getClass());
        
    public ResolutionService() {
    }
    
    @Auditable(actie = "aangeroepen", entiteitNaam = "ResolutionService")
    public String resolveGreeting(String name) {
        log.info("resolveGreeting wordt aangeroepen");
        return String.format("Welkom bij Spring beste %s!", name);
    }
    
    
    
    
}
