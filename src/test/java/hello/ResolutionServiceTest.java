/*
 * Developers.nl 2018
 */
package hello;

import org.apache.commons.logging.Log;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;
import org.junit.runner.RunWith;
import static org.mockito.Mockito.*;
import static org.mockito.Mockito.spy;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.EnableAspectJAutoProxy;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.util.ReflectionTestUtils;

/**
 * Lightweight test class. Just to test the running of the 
 * AuditAspect without loading the full spring boot application(context)
 * 
 * @author Marc Remijn, m.remijn@developers.nl
 */
@RunWith(SpringRunner.class)
@ContextConfiguration
public class ResolutionServiceTest {
    
    public ResolutionServiceTest() {
    }
    
    @Configuration
    @EnableAspectJAutoProxy
    static class Config {

        @Bean Log spyLog() {
            return spy(Log.class);
        }
        
        @Bean
        public ResolutionService resolutionService() {
            return new ResolutionService();
        }
        
        @Bean
        public AuditAspect auditAspect() {
            AuditAspect auditAspect = new AuditAspect();
            ReflectionTestUtils.setField(auditAspect, "log", spyLog());
            return auditAspect;
        }
    }
    
    @Autowired
    private ResolutionService resolutionService;
    
    @Autowired
    private Log spyLog;
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of resolveGreeting method, of class ResolutionService.
     */
    @Test
    public void testResolveGreeting() {
        String expectedFirstLogMessage = "Voor een resolution";
        String name = "Marc";
        String result = resolutionService.resolveGreeting(name);
        assertEquals("Welkom bij Spring beste Marc!", result);
        verify(spyLog).info(expectedFirstLogMessage);
        verify(spyLog, atLeast(1)).info("Method werd aangeroepen met als eerste argument: Marc");
    }
    
}
